<?php

namespace Tests\Feature\BookTest;

use Tests\TestCase;
use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Database\Seeders\CategorySeeder;

class UpdateBookTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(CategorySeeder::class);     
    }

    /** @test */
    public function update_book()
    {
        $this->handleValidationExceptions();
        // prepare 
        $book = Book::factory()->create();
        $new_data = Book::factory()->make();
    
        // act
        $response = $this->json('PUT', "/books/{$book->slug}", $new_data->toArray());

        // confirm
        $response->assertJson(['success' => true]);

        // verify
        $this->assertDatabaseHas('books', $new_data->toArray());
    }
}
