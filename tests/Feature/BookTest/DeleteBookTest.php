<?php

namespace Tests\Feature\BookTest;

use Tests\TestCase;
use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Database\Seeders\CategorySeeder;

class DeleteBookTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(CategorySeeder::class);     
    }

    /** @test */
    public function delete_book()
    {
        $this->handleValidationExceptions();
        // prepare 
        $book = Book::factory()->create();
    
        // act
        $response = $this->json('DELETE', "/books/{$book->slug}");

        // confirm
        $response->assertJson(['success' => true]);

        // verify
        $this->assertSoftDeleted('books', ['id'=> $book->id, 'slug'=> $book->slug]);
    }
}
