<?php

namespace Tests\Feature\BookTest;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Book;
use Database\Seeders\CategorySeeder;

class StoreBookTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(CategorySeeder::class);     
    }

    /** @test */
    public function store_book()
    {
        $this->handleValidationExceptions();
        //prepare
        $book = Book::factory()->make()->toArray();
        
        //act
        $response = $this->json('POST', '/books', $book);

        //confirm
        $response->assertJson(['success' => true]);

        //verify
        $this->assertDatabaseHas('books', $book);
    }
}
