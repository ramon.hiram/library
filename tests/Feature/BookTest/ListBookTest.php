<?php

namespace Tests\Feature\BookTest;

use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Database\Seeders\CategorySeeder;

class ListBookTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(CategorySeeder::class);     
    }

    /** @test */
    public function show_all_books()
    {
        // prepare
        $book = Book::factory()->count(10)->create();

        // act
        $response = $this->json('GET', '/books');

        // confirm
        $response->assertJson(Book::all()->toArray());
    }

     /** @test */
     public function show_a_book()
     {
         // prepare
        $book = Book::factory()->create();

        // act
         $response = $this->json('GET',"/books/{$book->slug}");

        // confirm
         $response->assertJson($book->toArray());

     }
}

