<?php

use App\Models\Book;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index')->name('books.index');



// Books Routes
Route::prefix('books')->group(function () {
    Route::get('/', 'BookController@index')->name('books.index');
    Route::get('/create', 'BookController@create')->name('books.create');
    Route::post('/store', 'BookController@store')->name('books.store');
    Route::get('/edit/{book}', 'BookController@edit')->name('books.edit');
    Route::put('/update/{book}', 'BookController@update')->name('books.update');
    Route::get('/show/{book}', 'BookController@show')->name('books.show');
    Route::delete('delete/{book}', 'BookController@destroy')->name('books.delete');
    
});