<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Library</title>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    	<link rel="stylesheet" href="{{ asset('/css/sidenav.scss') }}">
	</head>
	<body>
	  <nav>
		<div class="nav-wrapper">
			<div class="row">
			<div class="col s12">
						<a href="#" data-target="slide-out" class="sidenav-trigger show-on-large"><i class="material-icons">menu</i></a>
				<a href="https://codepen.io/collection/nbBqgY" target="_blank" class="brand-logo">Library</a>
						<ul class="right">
				</ul>
			</div>
			</div>
		</div>
		</nav>
		<ul id="slide-out" class="sidenav">
			<li><a href="{{ route('books.index') }}">Book List</a></li>
    		<li><a href="{{ route('books.create') }}">Create New Book</a></li>
		</ul>
        <main role="main" class="container">
            @yield('content')
        </main>
	<!-- Compiled and minified JavaScript -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		// SIDENAV
            $('.sidenav').sidenav();
	</script>
	@yield('scripts')
		
	</body>
	<footer>
	</footer>

</html>
