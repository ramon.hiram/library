@extends('layout')

@section('title', 'New book')
@section('content')

     <div class="row">
        <div class="section">
            @if(isset($edit))
            <form id="form_book" class="col s12" method="POST" action="{{ route('books.update', $book->slug) }}">
            @method('PUT')
            @else
            <form id="form_book" class="col s12" method="POST" action="{{ route('books.store') }}">
            @endif
            @csrf
                <div class="row">
                    <div class="input-field col s12">
                    <input id="name" name="name" type="text" class="validate" value="{{ $book->name ?? old('name') }}" @if(isset($show)) disabled @endif required>
                    <label for="name">Book Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="author" name="author" type="text" class="validate" value="{{ $book->author ?? old('author') }}" @if(isset($show)) disabled @endif required>
                    <label for="author">Author Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <select id="category_id" name="category_id" @if(isset($show)) disabled @endif class="validate"required>
                    <option value="" disabled selected>Choose your option</option>
                        @foreach ($categories as $category)
                            <option value="{{$category->id}}" @isset($book->category_id) @if($category->id == $book->category_id) selected @endif @endisset>{{$category->name}}</option>
                        @endforeach
                    </select>
                    <label>Category</label>
                </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="published_date" name="published_date" type="text" class="datepicker validate" value="{{ $book->published_date ?? old('published_date') }}" @if(isset($show)) disabled @endif required >
                    <label for="published_date">Published Date</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="user" name="user" type="text" value="{{ $book->user ?? '' }}" @if(isset($show)) disabled @endif>
                    <label for="user">User</label>
                    </div>
                </div>
                <div class="row">
                        <input id="available" name="available" value="{{$book->available ?? 0}}" hidden>
                    <label>
                        <input id="check_available" name="check_available" type="checkbox" class="filled-in" @isset($book->available) @if($book->available===1) checked @endif @endisset @if(isset($show)) disabled @endif />
                        <span>Available</span>
                    </label>
                </div>
                <div class = "row">
                 <button class="btn waves-effect waves-light right" type="submit" name="action" @if(isset($show)) disabled @endif>Save
                    <i class="material-icons right">send</i>
                </button>
                <a class="btn waves-effect waves-light red lighten-1 left" href="{{route('books.index')}}">Return
                    <i class="material-icons right">keyboard_return</i>
                </a>
                </div>
            </form>
        </div>
    </div>
    
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('select').formSelect();
            $('select[required]').css({
                display: 'inline',
                position: 'absolute',
                float: 'left',
                padding: 0,
                margin: 0,
                border: '1px solid rgba(255,255,255,0)',
                height: 0, 
                width: 0,
                top: '2em',
                left: '3em',
                opacity: 0
                });
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });
            $('#check_available').change(function() {
                if(this.checked) {
                    $('#available').val(1);
                }
                else
                $('#available').val(0);        
            });

            $( "#form_book" ).validate({
                    submitHandler: function(form) {
                            form.submit();
                        },
                    errorElement : 'div',
                    errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                    }
                });

        });
    </script>
@endsection
