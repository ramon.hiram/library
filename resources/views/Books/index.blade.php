@extends('layout')

@section('content')
@if (Session::has('message'))
    <body onload="M.toast({html: '{{ Session::get('message') }}'})">
@endif
<div class="row col s12"><a class="btn waves-effect waves-light blue darken-1 right" href="{{route('books.create')}}">Create a Book</a><div>
    @foreach ($books as $book )
        <div class="col s12 ">
          <div class="card hoverable" >
            {{-- <div class="card-image">
              <img src="images/sample-1.jpg">
              <span class="card-title"></span>
            </div> --}}
            <div class="card-content">
              <h3>{{$book->name}}</h3>
              <h4>{{$book->author}}</h4>            
              <a href="{{ route('books.show', $book->slug) }}" class="btn waves-effect waves-light" name="action">Show details
                <i class="material-icons right">play_arrow</i>
              </a>
            </div>
            <div class="card-action">
              <form id="delete_form" name="delete_form[]" action="{{route('books.delete', $book->slug)}}" method="POST" >
              <a href="{{ route('books.edit', $book->slug) }}"class="btn-floating amber waves-effect waves-light"><i class="material-icons">edit</i></a>
              @method('DELETE')
              @csrf
              <a id="delete" name="delete" class="deleteBtn btn-floating  red darken-3 waves-effect waves-light"> 
              <i class="material-icons right">clear</i>
              </a>
              </form>
            </div>
            
          </div>
        </div>
    @endforeach
@endsection

@section('scripts')
      <script> 
          $('.deleteBtn').on('click', function(e) {
              form = $(e.target).closest('a').parent('form[name="delete_form[]"]');
              console.log(form);
              swal({
                title: "Are you sure you want to delete this book?",
                text: "Once deleted, you can recover it since it's soft deleted!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((result) => {
                  if(result)
                  {
                    form.submit();
                  }
              });
            
        });
      </script>
@endsection
