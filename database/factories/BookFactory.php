<?php

namespace Database\Factories;

use App\Models\{Book, Category};
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'author' => $this->faker->name,
            'published_date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'category_id' => Category::inRandomOrder()->first()->id,
        ];
    }
}
