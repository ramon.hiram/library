<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 150);
            $table->string('author', 150);
            $table->unsignedBigInteger('category_id')->nullable();
            $table->date('published_date');
            $table->string('user')->nullable()->comment('Person that borrowed the book'); 
            $table->boolean('available')->default(0);
            $table->string('slug')->unique();
            //Column for softdeletes
            $table->softDeletes();
            $table->timestamps();

            // foreign keys
            $table->foreign('category_id')
                    ->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
