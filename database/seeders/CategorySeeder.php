<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ["Arts & Photography","Biographies & Memoirs","Business & Money","Calendars","Children's Books","Christian Books & Bibles","Comics & Graphic Novels",
        "Computers & Technology","Cookbooks, Food & Wine","Crafts, Hobbies & Home","Education & Teaching","Engineering & Transportation","Health, Fitness & Dieting",
        "History","Humor & Entertainment","Law","Lesbian, Gay, Bisexual & Transgender Books","Literature & Fiction","Medical Books",
        "Mystery, Thriller & Suspense","Parenting & Relationships","Politics & Social Sciences","Reference","Religion & Spirituality",
        "Romance","Science & Math","Science Fiction & Fantasy","Self-Help","Sports & Outdoors","Teen & Young Adult","Test Preparation","Travel",];

        foreach ($categories as $category) {
            Category::create(['name' => $category]);
        }

    }
}
